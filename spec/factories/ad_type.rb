FactoryGirl.define do
  factory :ad_type do
    sequence(:name) { |n| "type #{n}" }
  end
end
