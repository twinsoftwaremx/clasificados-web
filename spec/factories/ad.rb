FactoryGirl.define do
  factory :ad do
    sequence(:title) { |n| "ad #{n}" }
    association :type, factory: :ad_type
    side 'Busco'
  end
end
