require 'rails_helper'

describe Ad do

  it "is valid with title, type and side" do
    type1 = AdType.create(name: 'type 1')
    
    ad = Ad.new(
      title: 'ad 1',
      type: type1,
      side: 'Busco')
    expect(ad).to be_valid
  end

  it "returns the most recent ads with a given city"

  it "returns only my ads" do
    user1 = create(:user)
    user2 = create(:user)

    create_list(:ad, 5, user: user1)
    create_list(:ad, 4, user: user2)

    expect(Ad.my_ads(user1).count).to eq 5
  end

  it "returns the most popular categories of ads" do
    type1 = AdType.create(id: 1, name: 'type 1')
    type2 = AdType.create(id: 2, name: 'type 2')

    ad1 = Ad.create(title: 'ad 1', type: type1, side: 'Busco')
    ad2 = Ad.create(title: 'ad 2', type: type1, side: 'Busco')
    ad3 = Ad.create(title: 'ad 3', type: type2, side: 'Busco')
    ad4 = Ad.create(title: 'ad 4', type: type1, side: 'Busco')


    result = []
    result << [Ad.most_popular_by_category.first.id, Ad.most_popular_by_category.first.total]
    result << [Ad.most_popular_by_category.last.id, Ad.most_popular_by_category.last.total]


    expect(result).to eq [[1, 3], [2, 1]]
   end

  it "is created with status pending" do
    ad = create(:ad)
    expect(ad.status).to eq AdStatus.pending
  end

  it "changes the status after published" do
    ad = create(:ad)
    ad.publish
    expect(ad.status).to eq AdStatus.published
  end

end
