require 'rails_helper'

describe AdCategory do

  it "is valid with name and type" do
    ad_type = AdType.create(name: 'Type 1')
    ad_category = AdCategory.new(name: 'Category 1', type: ad_type)
    expect(ad_category).to be_valid
  end

  it "is invalid without name" do
    ad_category = AdCategory.new(name: nil)
    ad_category.valid?
    expect(ad_category.errors[:name]).to include("no puede estar en blanco")
  end

  it "is invalid without a type" do
    ad_category = AdCategory.new(type: nil)
    ad_category.valid?
    expect(ad_category.errors[:type]).to include("no puede estar en blanco")
  end

  it "returns a list of results for a given type" do
    type1 = AdType.create(name: 'Type 1')
    type2 = AdType.create(name: 'Type 2')

    category1 = AdCategory.create(name: 'Category 1', type: type1)
    category2 = AdCategory.create(name: 'Category 2', type: type2)
    category3 = AdCategory.create(name: 'Category 3', type: type1)

    expect(AdCategory.by_type(type1)).to eq [category1, category3]
  end
end
