# == Schema Information
#
# Table name: ad_statuses
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class AdStatus < ActiveRecord::Base

  def self.pending
    AdStatus.where(name: 'Pendiente').first
  end

  def self.published
    AdStatus.where(name: 'Publicado').first
  end

end
