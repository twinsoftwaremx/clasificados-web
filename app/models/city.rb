# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  state_id   :integer
#  created_at :datetime
#  updated_at :datetime
#

class City < ActiveRecord::Base
  belongs_to :state

  # Param: state: the state object
  def self.by_state(state)
    @cities = City.where(state: state)
  end

  def self.get_city(city_id)
    City.find(city_id)
  end

  def full_name
    "#{self.name}, #{self.state.name}"
  end
end
