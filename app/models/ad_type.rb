# == Schema Information
#
# Table name: ad_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :text
#  created_at  :datetime
#  updated_at  :datetime
#

class AdType < ActiveRecord::Base
  has_many :categories, class_name: 'AdCategory', :foreign_key => 'type_id'

  def self.most_active(count=10)
    AdType.order(:name).take(count)
  end
end
