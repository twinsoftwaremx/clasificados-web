# == Schema Information
#
# Table name: ads
#
#  id             :integer          not null, primary key
#  title          :string(255)
#  text           :text
#  category_id    :integer
#  city_id        :integer
#  status_id      :integer
#  is_new         :boolean
#  price          :decimal(, )
#  published_date :datetime
#  user_id        :integer
#  type_id        :integer
#  side           :string(255)
#  views          :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Ad < ActiveRecord::Base
  before_create :set_default_values

  resourcify

  belongs_to :category, class_name: 'AdCategory'
  belongs_to :city
  belongs_to :status, class_name: 'AdStatus'
  belongs_to :type, class_name: 'AdType'
  belongs_to :user

  has_many :pictures, class_name: 'AdPicture'
  
  AD_SIDES = [SIDE_LOOKING = 'Busco', SIDE_OFFERING = 'Ofrezco']
  
  validates :side, inclusion: {in: AD_SIDES}

  # Methods for public access
  def self.recent(city_id, count=10, only_published=true)
    if only_published
      @ads = Ad.where(city_id: city_id, status: AdStatus.published).order('published_date asc').take(count)
    else
      @ads = Ad.where(city_id: city_id).order('published_date asc').take(count)
    end
  end

  def self.most_popular_by_category
    Ad.joins(:type).select('ad_types.id, count(ad_types.id) as total').
      group('ad_types.id').
      order('count(ad_types.id) desc')
  end

  # Methods for authorized users
  def self.my_ads(user, count=10)
    @ads = Ad.where(user: user).order('created_at asc').take(count)
  end

  def publish
    self.status = AdStatus.published
    self.published_date = Time.zone.now
  end

  private
  def set_default_values
    if self.published_date.nil?
      self.status = AdStatus.pending
    else
      self.status = AdStatus.published
    end
  end

end
