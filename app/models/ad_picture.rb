# == Schema Information
#
# Table name: ad_pictures
#
#  id                 :integer          not null, primary key
#  caption            :string(255)
#  ad_id              :integer
#  created_at         :datetime
#  updated_at         :datetime
#  photo_file_name    :string(255)
#  photo_content_type :string(255)
#  photo_file_size    :integer
#  photo_updated_at   :datetime
#

class AdPicture < ActiveRecord::Base
  belongs_to :ad

  has_attached_file :photo, styles: {medium: '300x300', thumb: '100x100'}, default_url: '/assets/images/noimage.png'
end
