# == Schema Information
#
# Table name: ad_categories
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  parent_category_id :integer
#  description        :text
#  type_id            :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class AdCategory < ActiveRecord::Base
  belongs_to :parent_category, class_name: 'AdCategory'
  belongs_to :type, class_name: 'AdType'

  validates :name, presence: true
  validates :type, presence: true

  def self.by_type(type)
    AdCategory.where(type: type).order(:name)
  end

end
