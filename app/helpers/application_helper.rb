module ApplicationHelper

  def current_city
    if cookies[:ciudad]
      city = City.get_city(cookies[:ciudad])
      city.name
    end
  end


end
