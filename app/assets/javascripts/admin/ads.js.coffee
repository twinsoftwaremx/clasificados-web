# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('#editAdButton').click (event) ->
    event.preventDefault()
    location.href = $(this).attr('data-link')

  $('#publishAdButton').click (event) ->
    event.preventDefault()
    $.ajax 
        url: $(this).attr('data-link'),
        type: 'POST'
        dataType: 'text'
        error: (jqXHR, textStatus, errorThrown) ->
          $('body').append "AJAX Error: #{textStatus}"
        success: (data, textStatus, jqXHR) ->
          $('#adStatus').html('Publicado')
          $('#publishAdButton').remove()

  $('#goListAdsButton').click (event) ->
    event.preventDefault()
    location.href = $(this).attr('data-link')
