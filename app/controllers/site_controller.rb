class SiteController < ApplicationController
  before_action :check_city, only: [:index]

  def index
    @ads = Ad.recent(cookies[:ciudad], 10)
    @types = AdType.most_active(10)
    @categories = AdCategory.most_active(10)
  end

  def select_city
    @states = State.includes(:cities).order('states.name, cities.name').references(:cities).load
  end

  def set_city
    unless params[:name].nil?
      city = City.where(name: params[:name]).first
      cookies[:ciudad] = city.id unless city.nil?
      redirect_to root_path
    end
  end

  private
  def check_city
    if cookies[:ciudad].nil?
      redirect_to select_city_path
    end
  end
end
