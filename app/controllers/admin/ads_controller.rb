class Admin::AdsController < Admin::ProtectedController
  before_action :set_record, only: [:show, :edit, :update, :destroy]
  before_action :fill_tables, only: [:new, :create, :edit, :update]

  def index
    @ads = Ad.all
  end

  def show
  end

  def new
    @ad = Ad.new
  end

  def create
    @ad = Ad.new(ad_params)

    if params[:city_id].nil? then
    else
      @ad.city = City.find(params[:city_id])
    end

    if params[:category_id].nil? then
    else
      @ad.category = AdCategory.where(id: params[:category_id]).first
    end

    if params[:publish] then
      @ad.published_date = Date.today
    end

    @ad.user = current_user

    respond_to do |format|
      if @ad.save
        format.html { redirect_to [:admin, @ad], notice: 'El anuncio se agregó correctamente.' }
        format.json { render action: 'show', status: :created, location: @ad }
      else
        format.html { render action: 'new' }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @cities = City.by_state(@ad.city.state)
    @categories = AdCategory.by_type(@ad.type)

    @selected_state = @ad.city.state.id
    @selected_city = @ad.city.id
    @selected_type = @ad.type.id
    @selected_category = @ad.category.id unless @ad.category.nil?
  end

  def update

    if params[:city_id].nil? then
    else
      @ad.city = City.find(params[:city_id])
    end

    if params[:category_id].nil? then
    else
      @ad.category = AdCategory.find(params[:category_id])
    end

    respond_to do |format|
      if @ad.update(ad_params)
        format.html { redirect_to [:admin, @ad], notice: 'El anuncio se modificó correctamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @ad.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @ad.destroy
    respond_to do |format|
      format.html { redirect_to admin_ads_path }
      format.json { head :no_content }
    end
  end

  def update_cities
    state = State.find(params[:state_id])
    @cities = state.cities.map { |city| [city.name, city.id] }.insert(0, 'Seleccione la ciudad')
  end

  def update_categories
    type = AdType.find(params[:type_id])
    @categories = type.categories.map { |category| [category.name, category.id]}.insert(0, 'Seleccione una categoría')
  end

  def publish
    ad = Ad.find(params[:id])
    ad.publish
    if ad.save
      render json: '1'
    else
      render ad.errors.full_messages.to_sentence
    end
  end

  private

  def set_record
    @ad = Ad.find(params[:id])
  end

  def fill_tables
    @categories = []
    @states = State.all
    @cities = []
    @types = AdType.all
  end

  def ad_params
    params.require(:ad).permit(:title, :text, :city_id, :price, :is_new, :type_id, :side)
  end

end
