class Admin::AccountController < Admin::ProtectedController
  def index
  	@user = current_user

    @states = State.all
    @cities = []

    if not @user.city.nil?
	    @cities = City.by_state(@user.city.state)
	    @selected_state = @user.city.state.id
	    @selected_city = @user.city.id
	  end

  end

  def update
  end
end
