class Admin::AdCategoriesController < Admin::ProtectedController
  before_action :set_record, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
  end

  def create

  end

  def edit
  end

  def update

  end

  def destroy

  end

  private

  def set_record
    @category = AdCategory.find(params[:id])
  end

  def ad_params
    params.require(:category).permit(:name, :description)
  end
end
