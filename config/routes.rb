ClasificadosWeb::Application.routes.draw do

  get "account/index"
  devise_for :users

  namespace 'admin' do
    resources :ads do
      member do
        post 'publish'
      end
    end
    resources :ad_categories
    get 'update_cities_with_state' => 'ads#update_cities', as: 'ads_update_cities'
    get 'update_categories_with_type' => 'ads#update_categories', as: 'ads_update_categories'

    get 'cuenta' => 'account#index', as: 'my_account'
    post 'guardar_datos_cuenta' => 'account#update', as: 'update_account'
  end

  get 'anuncios/:name/:id' => 'ads#show', as: 'show_ad_details'

  get 'site/seleccionar_ciudad' => 'site#select_city', as: 'select_city'
  get 'site/establecer_ciudad/:name' => 'site#set_city', as: 'set_city'

  root 'site#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
