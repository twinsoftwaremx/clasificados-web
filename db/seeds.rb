# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Country.delete_all
mexico = Country.create(name: 'México')

State.delete_all
yuc = State.create(name: 'Yucatán', country: mexico)
qroo = State.create(name: 'Quintana Roo', country: mexico)

City.delete_all
merida = City.create(name: 'Mérida', state: yuc)
valladolid = City.create(name: 'Valladolid', state: yuc)
chetumal = City.create(name: 'Chetumal', state: qroo)
cancun = City.create(name: 'Cancún', state: qroo)

AdStatus.delete_all
AdStatus.create(name: 'Pendiente')
AdStatus.create(name: 'Publicado')
AdStatus.create(name: 'Cancelado')

AdType.delete_all
producto = AdType.create(name: 'Producto')
AdType.create(name: 'Servicio')
AdType.create(name: 'Empleo')
AdType.create(name: 'Bienes Raices')

AdCategory.delete_all
cel = AdCategory.create(name: 'Celulares')
cel.type = producto
cel.save
bebes = AdCategory.create(name: 'Bebés')
bebes.type = producto
bebes.save
#AdCategory.create(name: 'Ropa', type:producto)
#AdCategory.create(name: 'Electrodomésticos', type:producto)
#AdCategory.create(name: 'Vehículos', type:producto)