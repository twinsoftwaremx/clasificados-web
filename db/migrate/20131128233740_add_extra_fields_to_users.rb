class AddExtraFieldsToUsers < ActiveRecord::Migration
  def change

    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :username, :string
    add_column :users, :city_id, :integer
    add_column :users, :is_enabled, :boolean

  end
end
