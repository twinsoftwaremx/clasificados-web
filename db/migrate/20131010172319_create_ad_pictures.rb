class CreateAdPictures < ActiveRecord::Migration
  def change
    create_table :ad_pictures do |t|
      t.string :caption
      t.references :ad, index: true

      t.timestamps
    end
  end
end
