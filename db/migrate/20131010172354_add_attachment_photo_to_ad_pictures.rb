class AddAttachmentPhotoToAdPictures < ActiveRecord::Migration
  def self.up
    change_table :ad_pictures do |t|
      t.attachment :photo
    end
  end

  def self.down
    drop_attached_file :ad_pictures, :photo
  end
end
