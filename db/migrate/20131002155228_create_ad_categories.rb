class CreateAdCategories < ActiveRecord::Migration
  def change
    create_table :ad_categories do |t|
      t.string :name
      t.references :parent_category, index: true
      t.text :description
      t.references :type, index: true

      t.timestamps
    end
  end
end
