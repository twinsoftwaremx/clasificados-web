class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|
      t.string :title
      t.text :text
      t.references :category, index: true
      t.references :city, index: true
      t.references :status, index: true
      t.boolean :is_new
      t.decimal :price
      t.datetime :published_date
      t.references :user, index: true
      t.references :type, index: true
      t.string :side # [busco, ofrezco]
      t.integer :views

      t.timestamps
    end
  end
end
